## Ejercicio - Número de Entradas

Define el método `user` que interactuará con el usuario para recibir cualquier valor hasta que el usuario escribe la palabra `"Ya"`. Al final este método regresa el número de entradas que hizo el usuario.
Probablemente necesitarás hacer uso de una estructura iterativa y busca cómo capturar el input del usuario desde el teclado.

```Ruby
#user method



#driver code

#=> Ejemplo de entradas 
#>uno
#>dos
#>tres
#>cuatro
#>cinco
#>seis
#>Ya

#=> Número de entradas del usuario: 7
p user
#=> 7
```